package ictgradschool.industry.lab05.ex04.farmmanager;

import ictgradschool.industry.lab05.ex04.farmmanager.animals.*;

import java.util.ArrayList;

public class Farm {
    private ArrayList<Animal> animals;
    private final int startingMoney;
    private int money;

    public Farm(int money) {
        this.money = money;
        this.startingMoney = money;

        // An ArrayList is like an array, except it can hold any number of elements.
        // We will properly learn how to use lists and other collections in a later lecture.
        this.animals = new ArrayList<>();
    }

    public Farm() {
        this(10000);
    }

    /**
     * Returns how much money the farm currently has.
     */
    public int getMoney() {
        return this.money;
    }

    /**
     * Returns how much money the farm started with.
     */
    public int getStartingMoney() {
        return this.startingMoney;
    }

    /**
     * Purchases an animal and bills the farm for it.
     *
     * @param animalName The name of the type of animal you wish to buy.
     *
     * @return true if the animal can be bought, false otherwise.
     */
    public boolean buyAnimal(String animalName) {
        Animal newAnimal;
        switch (animalName) {
            case "cow":
                newAnimal = new Cow();
                break;
            case "sheep":
                newAnimal = new Sheep();
                break;
            case "chicken":
                newAnimal = new Chicken();
                break;
            default:
                // Animal not recognized.
                return false;
        }

        int price = (int) (newAnimal.getValue() * 1.15);

        if (money >= price) {
            money -= price;
        } else {

            // Insufficient funds.
            return false;
        }

        animals.add(newAnimal);
        return true;
    }

    /**
     * Sells all of the stock on the farm.
     */
    public int sell() {
        // Calculate the value of all of your animals
        int totalPrice = 0;

        // The size() method of an ArrayList is the same as the length field of an Array.
        for (int i = 0; i < animals.size(); i++) {

            // Calling the get method on an ArrayList is the same as using the square braces on an Array.
            // For example, if animals were an Array, the next line would look like this:
            // Animal a = animals[i];
            Animal a = animals.get(i);
            totalPrice += a.getValue();
        }

        // Remove the animals from your farm and add their value to your money.
        // The clear() method of an ArrayList removes all items from it.
        animals.clear();
        money += totalPrice;
        return totalPrice;
    }

    /**
     * Go through each animal and if you have enough money to feed it,
     * subtract the cost to feed from your money and call the feed method on the animal.
     */
    public void feedAll() {
        //TODO

        for (int i = 0; i < animals.size(); i++) {
            Animal a = animals.get(i);
            if (money > a.costToFeed()) {
                money = money - a.costToFeed();
                a.feed();
            } else {
                printNotEnoughMoneyToFeed();
                break;

            }
        }
    }

    /**
     * Go through each animal and if it is of the type specified and you have enough money to feed it,
     * subtract the cost to feed from your money and call the feed method on the animal.
     */
    public void feed(String animalType) {
        //TODO
        for (int i = 0; i < animals.size(); i++) {         //loop all animals
            Animal a = animals.get(i);                     //get the number i animal
            if (animalType.equals(a.getName())) {          //check if the animalType is the same
                if (money > a.costToFeed()) {
                    money = money - a.costToFeed();         //deduct the cost to feed it
                    a.feed();
                } else {
                    printNotEnoughMoneyToFeed();
                    break;
                }

            }
        }
    }

    /**
     * Prints out a list of all of the stock on the farm.
     * If there are no animals in stock inform the user.
     */
    public void printStock() {
        //TODO
        for (int i = 0; i < animals.size(); i++) {          //loop all animals
            Animal a = animals.get(i);                      //get the number i animal
            System.out.println((i + 1) + ". " + a.toString());                //print out the animal name and its value
        }
    }

    public void tick() {
        for (int i = 0; i < animals.size(); i++) {
            Animal a = animals.get(i);
            a.tick();
        }
    }

    public void harvest() {
        for (int i = 0; i < animals.size(); i++) {
            Animal a = animals.get(i);
            if (a instanceof ProductionAnimals) {
                if (((ProductionAnimals) a).harvestable()) {
                    int earning = ((ProductionAnimals) a).harvest();
                    money = money + earning;
                    System.out.println("You earned $" + earning + " by harvesting a " + a.getName());
                }
            }
        }
    }

    public void printNotEnoughMoneyToFeed(){
        System.out.println("You don't have enough money to feed them all.");
        System.out.println("You only have $" + money + " left.");
    }
}
