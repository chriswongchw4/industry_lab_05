package ictgradschool.industry.lab05.ex04.farmmanager.animals;


public class Cow extends Animal implements ProductionAnimals {

    public Cow() {
        super("cow", 1000, 0);
    }


    @Override
    public void feed() {
        if (value < 1500) {
            value += 100;
        }
    }

    @Override
    public int costToFeed() {
        return 60;
    }


    @Override
    public boolean harvestable() {
        if (tick>=10){
            return true;
        }
        return false;
    }

    @Override
    public int harvest() {
        if (harvestable()){
        tick=0;
        return 20;
        }
        return 0;
    }
}
