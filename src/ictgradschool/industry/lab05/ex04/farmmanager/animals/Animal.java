package ictgradschool.industry.lab05.ex04.farmmanager.animals;

public abstract class Animal {

    protected int value;
    protected final String name;
    protected int tick;

    public Animal(String name, int value, int tick) {
        this.name = name;
        this.value = value;
        this.tick = tick;
    }

    public abstract void feed();

    public abstract int costToFeed();

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return name + " - $" + value;
    }

    public void tick() {
        tick++;
    }
}
