package ictgradschool.industry.lab05.ex04.farmmanager.animals;


public class Sheep extends Animal implements ProductionAnimals {

    public Sheep() {
        super("sheep", 800, 0);
    }

    @Override
    public void feed() {
        if (value < 1400) {
            value += 300;
        }
    }

    @Override
    public int costToFeed() {
        return 60;
    }


    @Override
    public boolean harvestable() {
        if (tick>=5){
            return true;
        }
        return false;
    }

    @Override
    public int harvest() {
        if (harvestable()){
            tick=0;
            return 50;
        }
        return 0;
    }
}
