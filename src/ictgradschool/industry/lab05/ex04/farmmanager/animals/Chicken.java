package ictgradschool.industry.lab05.ex04.farmmanager.animals;


public class Chicken extends Animal implements ProductionAnimals {


    public Chicken(){
        super("chicken",500,0);
    }
    @Override
    public boolean harvestable() {
        if (tick>=3){
            return true;
        }
        return false;
    }

    @Override
    public int harvest() {
        if (harvestable()){
            tick=0;
            return 15;
        }
        return 0;
    }

    @Override
    public void feed() {
        if (value < 800) {
            value += 100;
        }
    }

    @Override
    public int costToFeed() {
        return 20;
    }
}
