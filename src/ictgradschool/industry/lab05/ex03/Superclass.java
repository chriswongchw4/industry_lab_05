package ictgradschool.industry.lab05.ex03;

public class Superclass {
    public int x = 10;
    static int y = 10;

    Superclass() {
        x = y++;
    }

    public int foo() {
        return x;
    }

    public static int goo() {
        return y;
    }
}

