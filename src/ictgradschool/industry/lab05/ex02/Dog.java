package ictgradschool.industry.lab05.ex02;

/**
 * Represents a dog.
 *
 * TODO Make this class implement the IAnimal interface, then implement all its methods.
 */
public class Dog implements IAnimal{

    @Override
    public String sayHello() {
        String sound="woof woof";
        return sound;
    }

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public String myName() {
        String name="Bruno";
        return name;
    }

    @Override
    public int legCount() {
        int legNum=4;
        return legNum;
    }

}
