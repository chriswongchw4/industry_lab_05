package ictgradschool.industry.lab05.ex02;

/**
 * Main program for Exercise Two.
 */
public class ExerciseTwo {

    public void start() {

        IAnimal[] animals = new IAnimal[3];

        // TODO Populate the animals array with a Bird, a Dog and a Horse.
        animals[0] = new Bird();
        animals[1] = new Dog();
        animals[2] = new Horse();

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(IAnimal[] list) {
        // TODO Loop through all the animals in the given list, and print their details as shown in the lab handout.
        for (int x=0;x<list.length;x++){
            String mammal="";
            String name="";
            if(list[x] instanceof Bird){
                name="bird";
            }else if(list[x] instanceof Dog){
                name="dog";
            }else if(list[x] instanceof Horse){
                name="horse";
            }

            if (list[x].isMammal()){
                mammal = "mammal";
            }else{
                mammal = "non-mammal";
            }
            System.out.println(list[x].myName() + " the " + name + " says " + list[x].sayHello() + ".");
            System.out.println(list[x].myName() + " the " + name + " is a " + mammal + ".");
            System.out.println("Did I forget to tell you that I have " + list[x].legCount() + " legs.");


            if (list[x] instanceof IFamous){
                    System.out.println("This is a famous name of my animal type: " + ((IFamous)list[x]).famous() );
            }

            System.out.println("------------------------------------------------------------------------------------");
        }
        // TODO If the animal also implements IFamous, print out that corresponding info too.
    }

    public static void main(String[] args) {
        new ExerciseTwo().start();
    }
}
