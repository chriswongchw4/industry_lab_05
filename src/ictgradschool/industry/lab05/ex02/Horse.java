package ictgradschool.industry.lab05.ex02;

/**
 * Represents a horse.
 * <p>
 * TODO Make this implement IAnimal and IFamous, and provide appropriate implementations of those methods.
 */
public class Horse implements IAnimal, IFamous {

    @Override
    public String sayHello() {
        String sound = "neigh";
        return sound;
    }

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public String myName() {
        String name = "Mr. Ed";
        return name;
    }

    @Override
    public int legCount() {
        int legNum = 4;
        return legNum;
    }

    @Override
    public String famous() {
        String type = "PharLap";
        return type;
    }


}
