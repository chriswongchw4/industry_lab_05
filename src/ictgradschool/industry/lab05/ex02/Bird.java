package ictgradschool.industry.lab05.ex02;

/**
 * Represents a Bird.
 *
 * TODO Correctly implement these methods, as instructed in the lab handout.
 */
public class Bird implements IAnimal {

    @Override
    public String sayHello() {
        String sound="Tweet Tweet";
        return sound;
    }

    @Override
    public boolean isMammal() {
        return false;
    }

    @Override
    public String myName() {
        String name="Tweety";
        return name;
    }

    @Override
    public int legCount() {
        int legNum=2;
        return legNum;
    }
}
